package tests;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import pages.BasePage;
import pages.Login;
import pages.MainPage;

public class AmazonApp extends BasePage{
	String parentWindow="";
	Login loginPage;
	MainPage mainPage;
	
	@Test(priority=1)
	public void AmazonTestcase1() throws InterruptedException {
		loginPage=new Login();
		mainPage= new MainPage();
		loginPage.LogintoAamzon("UserName","Password");
		mainPage.SearchProduct("JBL C50HI Wired in Ear Earphones with Mic (Black)");
		   String parentWindow=driver.getWindowHandle();
		    System.out.println("Parent window id is"+parentWindow);

			mainPage.findproduct.click();
			Set<String> allwindows=driver.getWindowHandles();
			
			int count =allwindows.size();
			
			System.out.println("Total windows are"+count);
			
			for(String child:allwindows) {
				
				if(!parentWindow.equals(child)) {
					driver.switchTo().window(child);
				}
			}
			
			Thread.sleep(5000);
			
			mainPage.addtocart.click();
			mainPage.navcart.click();
			
			String initialPrice=mainPage.intialprice.getText().trim();	
			System.out.println(initialPrice);
			Select select = new Select(mainPage.quantity);
			select.selectByVisibleText("10+");
			mainPage.productquantity.sendKeys("10");
			mainPage.upatedproduct.click();
			Thread.sleep(3000);
			 String finalPrice=mainPage.intialprice.getText().trim();	
			 System.out.println(finalPrice);
		     if(finalPrice.equals(initialPrice)) {
		    	 System.out.println("FAILED : Totasl Price not Changed");
		     }else {
		    	 System.out.println("PASSED :Total Price changed" );
		    	
		     }
		     
		     mainPage.Deleteproduct.click();
		     
		     if(mainPage.subtotalprice.isDisplayed()) {
		    	 System.out.println("PASSED : removed items from cart");
		     }else {
		    	 System.out.println("FAILED : not removed items from cart");
		     }
		     
		     driver.close();

		     driver.switchTo().window(parentWindow);
		    
		     Actions mouseAction = new Actions(driver);
		     mouseAction.moveToElement(mainPage.youraccount).build().perform();
		     mainPage.signout.click();
	
	}
	 
	
}
