package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Login extends BasePage {

	public Login() {
		PageFactory.initElements(driver, this);
	}
		
	@FindBy(linkText ="Sign in") WebElement signin; 
	@FindBy(xpath ="//*[@id='ap_email']") WebElement emailid;
	@FindBy(xpath ="//*[@class='a-button-input']") WebElement button;
	@FindBy(xpath ="//*[@id='ap_password']") WebElement password;
	@FindBy(xpath ="//*[@id='signInSubmit']") WebElement submit;

	public void LogintoAamzon(String emailid1,String password1) {
		signin.click();
		emailid.sendKeys(emailid1);
		button.click();
		password.sendKeys(password1);
		submit.click();	
		
	}
}
