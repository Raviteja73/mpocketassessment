package pages;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import webUtils.Browsers;

public class BasePage {
	public static WebDriver driver;
	
	@BeforeSuite
	public void setup() {
		driver=Browsers.startApplication(driver, "Chrome", "https://www.amazon.in/");
	}
	
	@AfterSuite
	public void teardown() {
		driver.quit();
	}

}
