package pages;

import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage extends BasePage{
	public MainPage() {
		PageFactory.initElements(driver, this);
	}
	
	//searchproduct
	
	@FindBy(xpath="//*[@type='text']") public WebElement productname;
	@FindBy(xpath="//*[@id='nav-search-submit-button']") public WebElement searchproudt;
	@FindBy(linkText ="JBL C50HI Wired in Ear Earphones with Mic (Black)" ) public WebElement findproduct;
	
	//add to cart
	@FindBy(xpath="//*[@name='submit.add-to-cart']") public WebElement addtocart;
	@FindBy(id="nav-cart") public WebElement navcart;
	
	//increasecount
	
	@FindBy(xpath="//*[@id='sc-subtotal-amount-buybox']") public WebElement intialprice;
	@FindBy(id="quantity")  public WebElement quantity;
	@FindBy(name="quantityBox") public WebElement productquantity;
	@FindBy(linkText ="Update" ) public WebElement upatedproduct;
	@FindBy(xpath="(//input[@value='Delete'])[1]") public WebElement Deleteproduct;
	@FindBy(xpath="//*[@id='sc-subtotal-label-activecart' and  contains(text(),'Subtotal (0 items):')]") public WebElement subtotalprice;
	@FindBy(xpath="//a[@id='nav-link-accountList']") public WebElement youraccount;
	@FindBy(xpath="//a[@id='nav-item-signout']") public WebElement signout;
	
	
	
	public void SearchProduct(String product) throws InterruptedException {
		Thread.sleep(3000);
		productname.sendKeys(product);
		searchproudt.click();
		
		
	}
	

}
